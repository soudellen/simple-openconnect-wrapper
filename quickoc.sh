#!/bin/bash
#################################################################################################################
# Quickly connect, do your stuff and disconnect from OpenConnect compliant VPN server.
#
# Examble usage:
#   Connect to VPN
#   $ ./quickoc.sh connect myusername mypassword https://vpn.examble.com/
#
#   Do some stuff on the remote site
#   $ ssh myusername@server.examble.com
#   ...
#   $ exit
#   
#   Disconnect from VPN
#   $ ./quickoc.sh disconnect
# 
# NOTE: You can have only one connection at a time. 
#
# WARNING: The script needs to be run as a privileged user inorder to allow OpenConnect to create a tun device.
#   For options to run openconnect without root privileges see: http://www.infradead.org/openconnect/nonroot.html

usage() {
  echo "Usage: ./$(basename $0) connect USER PASSWORD VPN_URL EXTRA_OPTIONS"
  echo "       ./$(basename $0) disconnect" 
}

PID_FILE="/var/run/openconnect_${BASHPID}.pid"

# Connect to VPN
connect() {
  VPN_USER=$1
  VPN_PASS=$2
  VPN=$3
  openconnect \
    --user=${VPN_USER} \
    --passwd-on-stdin \
    --background \
    --pid-file=${PID_FILE} \
    ${@:4} \
    ${VPN} \
    <<< "${VPN_PASS}"
}

# Close the VPN Connection
disconnect() {
  _pid=$(cat ${PID_FILE})
  kill -s SIGINT $_pid
  while [ -e /proc/$_pid ]; do sleep 0.1; done
  rm -f ${PID_FILE}
}

main() {
  if [[ $UID != 0 ]]; then
    echo "You need root privileges to run this script"
    exit 1
  fi
  if ! [ -x "$(command -v openconnect)" ]; then
    echo "Error: openconnect is not installed."
    exit 1
  fi
  if [[ "$#" -ge 3 ]] && [[ $1 = "connect" ]]; then
    connect ${@:2}
  elif [[ "$#" -eq 1 ]] && [[ $1 = "disconnect" ]]; then
    disconnect
  else
    usage
    exit 1
  fi
}

main $@
